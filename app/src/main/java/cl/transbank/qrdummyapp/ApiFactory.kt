package cl.transbank.qrdummyapp

object ApiFactory {


    val transactionApi: TransactionApi = RetrofitFactory.retrofit().create(TransactionApi::class.java)

}