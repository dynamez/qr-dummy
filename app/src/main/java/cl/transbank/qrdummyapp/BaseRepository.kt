package cl.transbank.qrdummyapp

import android.util.Log
import retrofit2.Response
import java.io.IOException

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {

        val result: TransactionResult<T> = safeApiResult(call, errorMessage)
        var data: T? = null
        when (result) {
            is TransactionResult.Success ->
                data = result.data
            is TransactionResult.Error -> {
                Log.d("1.DataRepository", "$errorMessage % Exception - ${result.exception}")
            }
        }
        return data
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>,
        errorMessage: String
    ): TransactionResult<T> {
        val response = call.invoke()
        if (response.isSuccessful) return TransactionResult.Success(response.body()!!)

        return TransactionResult.Error(IOException("Error ocurred during getting safe Api result, Custom ERROR - $errorMessage"))

    }


}