package cl.transbank.qrdummyapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.mainscreen_fragment.*


class Mainscreen : Fragment() {

    companion object {
        fun newInstance() = Mainscreen()
    }

    private lateinit var viewModel: MainscreenViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.mainscreen_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainscreenViewModel::class.java)
        // TODO: Use the ViewModel

        scanQr.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_mainscreen_to_mainFragment)
        }

        insertTransactionQr.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_mainscreen_to_manualCode)
        }
    }

}
