package cl.transbank.qrdummyapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.manual_code_fragment.*


class ManualCode : Fragment() {

    companion object {
        fun newInstance() = ManualCode()
    }

    private lateinit var viewModel: ManualCodeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.manual_code_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ManualCodeViewModel::class.java)


        startTransactionBtn.setOnClickListener {
            if (!qrCode.text.isNullOrBlank()) {
                val bundle = Bundle()
                bundle.putString("transactionId", qrCode.text.toString())
                NavHostFragment.findNavController(this).navigate(R.id.action_manualCode_to_loadingQR, bundle)
            } else {
                Toast.makeText(context, "Inserte un código para continuar", Toast.LENGTH_SHORT).show()
            }


        }


    }

}
