package cl.transbank.qrdummyapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.payment_success_fragment.*


class PaymentSuccess : Fragment() {

    companion object {
        fun newInstance() = PaymentSuccess()
    }

    private lateinit var viewModel: PaymentSuccessViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.payment_success_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PaymentSuccessViewModel::class.java)
        // TODO: Use the ViewModel

        backBtn.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_paymentSuccess_to_mainscreen)
        }
    }

}
