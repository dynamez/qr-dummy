package cl.transbank.qrdummyapp

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface TransactionApi {
    @POST("transaction/update")
    suspend fun updateTransactionStatus(@Body model: CreateTransactionModel): Response<TransactionCreation>
}