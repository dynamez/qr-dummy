package cl.transbank.qrdummyapp

data class TransactionCreation(

    val data: String
)

data class TransactionResponse(
    val results: List<TransactionCreation>
)


