package cl.transbank.qrdummyapp


import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class TransactionRepository(
    private val api: TransactionApi
) : BaseRepository() {

    suspend fun postTransactionCreation(
        transactionId: String,
        transactionStatus: Long
    ): TransactionCreation? = withContext(Dispatchers.IO) {

        val model = CreateTransactionModel(
            transactionId,
            transactionStatus
        )
        safeApiCall(
            call = { api.updateTransactionStatus(model) },
            errorMessage = "Error creating Transaction"
        )


    }

   suspend fun payTransaction(
        transactionId: String,
        transactionStatus: Long
    ): TransactionCreation? = withContext(Dispatchers.IO) {

        val model = CreateTransactionModel(
            transactionId,
            transactionStatus
        )
        safeApiCall(
            call = { api.updateTransactionStatus(model) },
            errorMessage = "Error creating Transaction"
        )


    }

    suspend fun cancelTransaction(
        transactionId: String,
        transactionStatus: Long
    ): TransactionCreation? = withContext(Dispatchers.IO) {
        val model = CreateTransactionModel(
            transactionId,
            transactionStatus
        )
        safeApiCall(
            call = { api.updateTransactionStatus(model) },
            errorMessage = "Error canceling transaction"
        )
    }
}