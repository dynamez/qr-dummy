package cl.transbank.qrdummyapp

sealed class TransactionResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : TransactionResult<T>()
    data class Error(val exception: Exception) : TransactionResult<Nothing>()

}