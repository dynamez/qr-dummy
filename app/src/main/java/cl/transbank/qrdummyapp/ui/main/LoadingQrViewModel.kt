package cl.transbank.qrdummyapp.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import androidx.lifecycle.viewModelScope
import cl.transbank.qrdummyapp.ApiFactory
import cl.transbank.qrdummyapp.TransactionCreation
import cl.transbank.qrdummyapp.TransactionRepository
import kotlinx.coroutines.launch


class LoadingQrViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    private val repository: TransactionRepository = TransactionRepository(ApiFactory.transactionApi)
    val transactionLiveData = MutableLiveData<TransactionCreation>()
    val transactionPayedLiveData = MutableLiveData<TransactionCreation>()
    val transactionCanceledLiveData = MutableLiveData<TransactionCreation>()

    fun createTransaction(
        transactionId: String,
        transactionStatus: Long
    ) {
        viewModelScope.launch {
            val transactions = repository.postTransactionCreation(
                transactionId, transactionStatus
            )
            transactionLiveData.postValue(transactions)
        }
    }

    fun payTransaction(
        transactionId: String,
        transactionStatus: Long
    ) {
        viewModelScope.launch {
            val transactions = repository.payTransaction(
                transactionId, transactionStatus
            )
            transactionPayedLiveData.postValue(transactions)
        }
    }

    fun cancelTransaction(
        transactionId: String,
        transactionStatus: Long
    ) {
        viewModelScope.launch {
            val transactions = repository.cancelTransaction(
                transactionId,
                transactionStatus
            )
            transactionCanceledLiveData.postValue(transactions)
        }
    }
}
