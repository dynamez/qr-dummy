package cl.transbank.qrdummyapp.ui.main

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import cl.transbank.qrdummyapp.R
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class MainFragment : Fragment(), ZXingScannerView.ResultHandler {

    companion object {
        fun newInstance() = MainFragment()
        const val TAG = "QRSCAN"
        const val CAMERA_PERMISION_CODE = 1
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var waitingDialog: AlertDialog
    private lateinit var mScannerView: ZXingScannerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mScannerView = ZXingScannerView(activity)
        return mScannerView
        //return inflater.inflate(R.layout.qrscan_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
//        mScannerView = ZXingScannerView(context!!)


        if (PermissionChecker.checkSelfPermission(context!!, Manifest.permission.CAMERA)
            != PermissionChecker.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                val builder = AlertDialog.Builder(context!!)
                builder.setMessage("Permission to access the camera is required for this app to record audio.")
                        .setTitle("Permission required")

                            builder.setPositiveButton("OK"
                            ) { dialog, id ->
                        Log.i(TAG, "Clicked")
                                requestPermissions(
                                    arrayOf(android.Manifest.permission.CAMERA),
                                    CAMERA_PERMISION_CODE
                                )
                    }

                    val dialog = builder.create()
                dialog.show()
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(
                    arrayOf(android.Manifest.permission.CAMERA),
                    CAMERA_PERMISION_CODE
                )

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            mScannerView.startCamera()
        }


        //waitingDialog = SpotsDialog.Builder().setContext(context).setMessage("Please Wait").setCancelable(false).build()

//        btndtec.setOnClickListener {
////            camera_view.captureImage(CameraKitView.ImageCallback(
////            ))
//

////            Log.i(TAG, "onActivity created")
////            camera_view.captureImage { cameraKitView, byteArray ->
////                Toast.makeText(context,"You captured an image!", Toast.LENGTH_LONG).show()
////                val bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray!!.size)
////                runDetector(bitmap)
////                graphicOverlay.clear()
////            }
//
//
//
//        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//         camera_view.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CAMERA_PERMISION_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //fetchMethod()
                mScannerView.startCamera()

            } else {
                Toast.makeText(context, "Permission not granted!", Toast.LENGTH_SHORT).show()
            }

            else -> {
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume")
        mScannerView.setResultHandler(this)
        mScannerView.startCamera()
//        camera_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
        Log.i(TAG, "onPause")
//         camera_view.onPause()
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, "onStart")
//        camera_view.onStart()
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "onStop")
//         camera_view.onStop()
    }


    override fun handleResult(rawResult: Result?) {

        Log.e("handler", rawResult?.text!!)


        Toast.makeText(context, "QR ${rawResult.text!!}", Toast.LENGTH_LONG).show()
        mScannerView.stopCamera()
        val fragment = loadingQR()
        val args = Bundle()
        args.putString("transactionId", rawResult.text)
        args.putString("transactionStatus", "1")
        
        NavHostFragment.findNavController(this).navigate(R.id.action_mainFragment_to_loadingQR, args)


    }


}