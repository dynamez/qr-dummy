package cl.transbank.qrdummyapp.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import cl.transbank.qrdummyapp.R
import kotlinx.android.synthetic.main.loading_qr_fragment.*

class loadingQR : Fragment() {

    companion object {
        fun newInstance() = loadingQR()
    }

    private lateinit var viewModel: LoadingQrViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.loading_qr_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoadingQrViewModel::class.java)
        // TODO: Use the ViewModel
        viewModel.transactionLiveData.observe(viewLifecycleOwner, Observer {
            Log.d("example", it.toString())
            statusLabel.text = "Informado con éxito"
            progressBar.visibility = View.INVISIBLE
            pagarBtn.isEnabled = true
        })
        viewModel.transactionPayedLiveData.observe(viewLifecycleOwner, Observer {
            Log.d("pay response:", it.toString())
            NavHostFragment.findNavController(this).navigate(R.id.action_loadingQR_to_paymentSuccess)
        })
        viewModel.transactionCanceledLiveData.observe(viewLifecycleOwner, Observer {
            NavHostFragment.findNavController(this).navigate(R.id.action_loadingQR_to_mainscreen)
        })

        viewModel.createTransaction(arguments?.getString("transactionId")!!, 2)
˚ˆˆ
        pagarBtn.setOnClickListener {
            viewModel.payTransaction(arguments?.getString("transactionId")!!, 3)
        }˚

        cancelTransactionBtn.setOnClickListener {
            viewModel.cancelTransaction(arguments?.getString("transactionId")!!, 4)
        }
    }

}
